From: Ben Hutchings <ben@decadent.org.uk>
Date: Sun, 15 Jul 2018 21:27:09 +0100
Subject: [klibc] x86_64: Reduce ld max-page-size option again
Bug-Debian: https://bugs.debian.org/903849
Forwarded: https://www.zytor.com/pipermail/klibc/2018-July/003998.html

Building klibc with binutils 2.30 results in the following layout
for klibc.so:

Idx Name          Size      VMA               LMA               File off  Algn
  0 .text         0000d1ff  0000000000200200  0000000000200200  00000200  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
  1 .rodata       000031af  0000000000300000  0000000000300000  00100000  2**5
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  2 .data.rel.ro  00000990  0000000000403660  0000000000403660  00103660  2**5
                  CONTENTS, ALLOC, LOAD, DATA
  3 .data         00000140  0000000000404000  0000000000404000  00104000  2**5
                  CONTENTS, ALLOC, LOAD, DATA
  4 .bss          000042f8  0000000000404140  0000000000404140  00104140  2**5
                  ALLOC

The .text and .rodata sections have each been padded to the specified
maximum page size of 1 MB, causing the following sections to overlap
the executable.

This causes all klibc executables to crash immediately.

Since there's no architectural page size betwen 4 KB and 2MB, set
max-page-size to 4 KB.

Signed-off-by: Ben Hutchings <ben@decadent.org.uk>
---
--- a/usr/klibc/arch/x86_64/MCONFIG
+++ b/usr/klibc/arch/x86_64/MCONFIG
@@ -30,10 +30,12 @@ KLIBCLDFLAGS      = -m elf_x86_64
 # calls, and work on the memory models for this architecture
 # 2 MB - normal binaries start at 4 MB
 #
-# Recent binutils use max-page-size=0x200000 by default, which pushes
+# binutils now uses max-page-size=0x200000 by default, which pushes
 # klibc.so data over the 4 MB mark, overlapping the executable.
-# Revert to the old max-page-size=0x100000 value.
-KLIBCSHAREDFLAGS     = -Ttext 0x00200200 -z max-page-size=0x100000
+# The old default was max-page-size=0x100000, but that also results
+# in a broken layout with binutils 2.30.  Since there's no
+# architectural page size betwen 4 KB and 2MB, set it to 4 KB.
+KLIBCSHAREDFLAGS     = -Ttext 0x00200200 -z max-page-size=0x1000
 
 # Asm includes for x86_64 are in the merged x86 tree
 KLIBCARCHINCFLAGS = -I$(KLIBCKERNELOBJ)/arch/x86/include

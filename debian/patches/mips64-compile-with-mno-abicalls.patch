From: James Cowgill <james.cowgill@mips.com>
Date: Fri, 2 Mar 2018 14:48:21 +0000
Subject: mips64: compile with -mno-abicalls
Forwarded: https://www.zytor.com/pipermail/klibc/2018-March/003985.html
Bug-Debian: https://bugs.debian.org/891924

By default, the MIPS toolchain compiles all code as PIC. Since klibc
links everything at static addresses, we don't need PIC code so use
-mno-abicalls to disable it. To fix subsequent link errors, use
-Ttext-segment to adjust the base address of klibc to a more sensible
location.

This fixes a bug in the shared library form of klibc where programs
would segfault in the syscall handler because we tried to store into the
"errno" variable without setting up the gp register. This is only required
under the PIC ABI.

Signed-off-by: James Cowgill <james.cowgill@mips.com>
---
 usr/klibc/arch/mips64/MCONFIG | 12 +++++++++++-
 1 file changed, 11 insertions(+), 1 deletion(-)

diff --git a/usr/klibc/arch/mips64/MCONFIG b/usr/klibc/arch/mips64/MCONFIG
index b37cc6a7..6a4b41b2 100644
--- a/usr/klibc/arch/mips64/MCONFIG
+++ b/usr/klibc/arch/mips64/MCONFIG
@@ -7,7 +7,17 @@
 # accordingly.
 #
 
+KLIBCARCHREQFLAGS = -fno-pic -mno-abicalls -G 0
 KLIBCOPTFLAGS += -Os
 KLIBCBITSIZE  = 64
 
-KLIBCSHAREDFLAGS  = -T $(src)/arch/mips/klibc.ld
+# Extra linkflags when building the shared version of the library
+# This address needs to be reachable using normal inter-module
+# calls, and work on the memory models for this architecture
+# 4862 MB - normal binaries start at 4608 MB. Non-PIC jumps usually
+# use the JAL instruction which requires a destination within the same
+# 256M aligned region. Since we can't put ourselves below the normal
+# load address, use the very top of the 256M region (minus 2MB)
+#
+# Use -Ttext-segment so that the special .MIPS* sections are moved as well.
+KLIBCSHAREDFLAGS     = -Ttext-segment 0x12FE00000
-- 
2.16.2

